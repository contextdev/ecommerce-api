from rest_framework import serializers
from ecommerce.cart import Cart, CartLine
from ecommerce.product.models import ProductVariant
from ecommerce.product.serializers import ProductVariantSerializer


class DataCartLineSerializer(serializers.Serializer):
    unit_price_net = serializers.FloatField(required=False)
    unit_price_gross = serializers.FloatField(required=False)
    product_id = serializers.IntegerField(required=False) #serializers.PrimaryKeyRelatedField(read_only=True)
    product_slug = serializers.SlugField(required=False)
    extra_data = serializers.ListField(required=False)
    variant_id = serializers.IntegerField(required=False)

    class Meta:
        fields = ('unit_price_net', 'unit_price_gross', 'product_id', 'product_slug', 'extra_data', 'variant_id')


class CartLineSerializer(serializers.Serializer):

    product = serializers.CharField()
    quantity = serializers.IntegerField()
    data = DataCartLineSerializer(read_only=True)

    class Meta:
        fields = ('product', 'quantity', 'data')
        model = CartLine


class CartSerializer(serializers.Serializer):
    cart_lines = CartLineSerializer(many=True)

    class Meta:
        fields = ('cart_lines',)


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""
    def to_internal_value(self, data):
        return data
    def to_representation(self, value):
        return value


class NewCartItemSerializer(serializers.Serializer):
    variant = ProductVariantSerializer()
    product = serializers.IntegerField()
    quantity = serializers.IntegerField()
    price = serializers.FloatField()
    msg = serializers.CharField()
    data = JSONSerializerField()
    sub_total = serializers.FloatField(read_only=True)
    sku = serializers.SerializerMethodField()

    def get_sku(self, obj):
        variant = ProductVariant.objects.get(id=obj.product)
        return variant.sku


class NewCartSerializer(serializers.Serializer):
    created_date = serializers.DateTimeField()
    updated_date = serializers.DateTimeField()
    items = NewCartItemSerializer(many=True)
    total = serializers.FloatField(read_only=True)

