from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from ecommerce.discount.api import SaleViewSet, VoucherViewtSet
from ecommerce.order.api import OrderViewSet, DeliveryGroupViewtSet, OrderedItemViewtSet, \
    OrderHistoryEntryViewtSet, OrderNoteViewtSet, PaymentViewtSet
from ecommerce.product.api import CategoryModelViewSet, ProductViewSet, ProductVariantSet, ProductImageViewSet
from ecommerce.shipping.api import ShippingMethodCountryViewtSet, ShippingMethodViewSet
from ecommerce.userprofile.api import AddressViewSet, UserViewtSet
from ecommerce.cart.api import NewCartViewSet

router = DefaultRouter(trailing_slash=False)
# Discount app
router.register(r'sales', SaleViewSet, base_name='sales')
router.register(r'vouchers', VoucherViewtSet, base_name='vouchers')
# Order app
router.register(r'orders', OrderViewSet, base_name='orders')
router.register(r'orders-items', OrderedItemViewtSet, base_name='orders-items')
router.register(r'delivery-groups', DeliveryGroupViewtSet, base_name='delivery-groups')
router.register(r'order-histories', OrderHistoryEntryViewtSet, base_name='order-histories')
router.register(r'order-notes', OrderNoteViewtSet, base_name='order-notes')
router.register(r'payments', PaymentViewtSet, base_name='payments')
# Product app
router.register(r'products', ProductViewSet, base_name='products')
router.register(r'products-variants', ProductVariantSet, base_name='products-variants')
router.register(r'categories', CategoryModelViewSet, base_name='categories')
router.register(r'product-images', ProductImageViewSet, base_name='product-images')
# Shippin app
router.register(r'shipping-methods', ShippingMethodViewSet, base_name='shipping-methods')
router.register(r'shipping-methods-country', ShippingMethodCountryViewtSet, base_name='shipping-methods-country')
# UserProfile app
router.register(r'addresses', AddressViewSet, base_name='addresses')
router.register(r'users', UserViewtSet, base_name='users')
router.register(r'new-cart', NewCartViewSet, base_name='new-cart')


urlpatterns = [
    url(r'^accounts/', include('rest_auth.urls')),
    url(r'^accounts/registration/', include('rest_auth.registration.urls')),
    url(r'^api-token-auth', obtain_jwt_token),
    # Include router api
    url(r'^', include(router.urls)),
    ]
