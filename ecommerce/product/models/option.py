from django.db import models
from .base import Category
from versatileimagefield.fields import VersatileImageField
from django.utils.translation import pgettext_lazy

# Create your models here.

class OptionSet(models.Model):
    TEXT = 'TXT'
    IMAGE = 'IMG'
    WIDEGET_TYPE_CHOICES = (
        (TEXT, 'Text'),
        (IMAGE, 'Image'),
    )

    title = models.CharField(max_length=40, unique=True)
    description = models.TextField()
    image = models.ImageField(upload_to='option_set', null=True, blank=True, )
    category = models.ForeignKey(Category, related_name='option_sets')
    required = models.BooleanField(default=False)
    widget_type = models.CharField(max_length=3, choices=WIDEGET_TYPE_CHOICES, default=TEXT)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Option Set"
        verbose_name_plural = "Option Sets"
        ordering = ["-title"]


class Option(models.Model):
    parent = models.ForeignKey(OptionSet, related_name='options')
    title = models.CharField(max_length=40, unique=True)
    description = models.TextField()
    short_description = models.CharField(max_length=255, blank=True)
    # image = models.ImageField(upload_to='option', null=True, blank=True, )
    image = VersatileImageField(
        pgettext_lazy('Option choice value field', 'image'),
        upload_to='option', blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Option"
        verbose_name_plural = "Options"
        ordering = ["-title"]



