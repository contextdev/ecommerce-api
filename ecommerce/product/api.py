from rest_framework import viewsets

from .models import Product, ProductVariant, Category, ProductImage

from .serializers import ProductSerializer, ProductVariantSerializer, CategorySerializer, ProductImageSerializer

import django_filters
from rest_framework import filters
from rest_framework import generics

class ProductVariantFilter(filters.FilterSet):
    stock_lte = django_filters.MethodFilter(action='filter_by_stock_lte', distinct=True)

    def filter_by_stock_lte(self, queryset, value):
        return queryset.filter(stock__quantity__lte=value)

    class Meta:
        model = ProductVariant
        #fields = ['stock']



class ProductViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing Products
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    search_fields = ('name',)

class ProductVariantSet(viewsets.ModelViewSet):
    """"
    A simple ViewSet for viewwing Variants
    """
    queryset = ProductVariant.objects.all()
    serializer_class = ProductVariantSerializer
    filter_fields = ('stock',)
    filter_class = ProductVariantFilter
    search_fields = ('product__name', 'name', 'sku')


class CategoryModelViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ProductImageViewSet(viewsets.ModelViewSet):
    queryset = ProductImage.objects.all()
    serializer_class = ProductImageSerializer
    filter_fields = ('product',)