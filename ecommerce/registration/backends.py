from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend


User = get_user_model()


class Backend(ModelBackend):

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class UsernamePasswordBackend(Backend):
    """Authentication backend that expects an email in username parameter."""

    def authenticate(self, username=None, password=None, **_kwargs):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return None
        if user.check_password(password):
            return user



class TrivialBackend(Backend):
    """Authenticate with user instance."""

    def authenticate(self, user=None, **_kwargs):
        if isinstance(user, User):
            return user
