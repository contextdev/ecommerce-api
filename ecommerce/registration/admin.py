from django.contrib import admin

from .models import EmailConfirmationRequest, EmailChangeRequest

admin.site.register(EmailChangeRequest)
admin.site.register(EmailConfirmationRequest)