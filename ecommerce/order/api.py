from rest_framework import viewsets

from .models import Order, DeliveryGroup, OrderedItem, Payment, OrderHistoryEntry, OrderNote

from .serializers import OrderSerializer, OrderedItemSerializer, DeliveryGroupSerializer, \
    PaymentSerializer, OrderHistoryEntrySerializer, OrderNoteSerializer


class OrderViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing Orders
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_fields = ('status', 'user')


class OrderedItemViewtSet(viewsets.ModelViewSet):
    """"
    A simple ViewSet for viewwing OrderedItems
    """
    queryset = OrderedItem.objects.all()
    serializer_class = OrderedItemSerializer
    filter_fields = ('order',)

class DeliveryGroupViewtSet(viewsets.ModelViewSet):
    """"
    A simple ViewSet for viewwing DeliveryGroups
    """
    queryset = DeliveryGroup.objects.all()
    serializer_class = DeliveryGroupSerializer

class PaymentViewtSet(viewsets.ModelViewSet):
    """"
    A simple ViewSet for viewwing Payments
    """
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    filter_fields = ('order',)

class OrderHistoryEntryViewtSet(viewsets.ModelViewSet):
    """"
    A simple ViewSet for viewwing OrderHistoryEntrys
    """
    queryset = OrderHistoryEntry.objects.all()
    serializer_class = OrderHistoryEntrySerializer

class OrderNoteViewtSet(viewsets.ModelViewSet):
    """"
    A simple ViewSet for viewwing OrderNotes
    """
    queryset = OrderNote.objects.all()
    serializer_class = OrderNoteSerializer