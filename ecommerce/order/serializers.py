from babeldjango.templatetags.babel import currencyfmt
from django.conf import settings
from rest_framework import serializers

from .models import Order, DeliveryGroup, OrderedItem, Payment, OrderHistoryEntry, OrderNote


class OrderedItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderedItem
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = '__all__'


class OrderedItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderedItem
        fields = '__all__'

class DeliveryGroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = DeliveryGroup
        fields = '__all__'

class PaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payment
        fields = '__all__'

class OrderHistoryEntrySerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderHistoryEntry
        fields = '__all__'

class OrderNoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderNote
        fields = '__all__'