from rest_framework import serializers


class BaseSerializer(serializers.ModelSerializer):

    class Meta:


    def __init__(self, *args, **kwargs):
        super(BaseSerializer, self).__init__(*args, **kwargs)
        user = kwargs['context']['request'].user
        action = self.context['view'].action

        # Filter fields depending on whether it's a sender or a recipient
        not_allowed_to_show = ()
        user_type = None
        # raise Exception(action)
        try:
            if action == 'inbox' or action == 'list' or action == 'trash' or user == self.instance.recipient:
                user_type = 'recipient'
            elif action == 'sent' or user == self.instance.sender:
                user_type = 'sender'
            else:
                raise Exception("User is neither sender nor recipient")
        except:
            # TODO: if its an empty list, it fails
            # TODO: change this
            user_type = 'sender'

        if user_type == 'recipient':
            not_allowed_to_show = ('recipient', 'sender_archived', 'sender_deleted_at', 'sender', 'sender_deleted',
                                   'moderation_status', 'moderation_reason', 'moderation_date')
        elif user_type == 'sender':
            not_allowed_to_show = ( 'read_at', 'replied_at', 'is_replied', 'recipient_deleted',
                                    'recipient_archived', 'recipient_deleted_at')
            self.fields['recipient'].write_only = True

        if action == 'create' or action == 'reply':
            self.fields['is_new'].read_only = True
            self.fields['sender_deleted'].read_only = True
            self.fields['recipient_deleted'].read_only = True
        else:
            self.fields['subject'].read_only = True
            self.fields['body'].read_only = True

        if action == 'reply':
            self.fields['object_id'].read_only = True
            self.fields['object_id'].required = False

        for field_name in not_allowed_to_show:
            self.fields.pop(field_name)

    def get_field_names(self, declared_fields, info):
        """
        Returns the list of all field names that should be created when
        instantiating this serializer class. This is based on the default
        set of fields, but also takes into account the `Meta.fields` or
        `Meta.exclude` options if they have been specified.
        """
        fields = getattr(self.Meta, 'fields', None)
        exclude = getattr(self.Meta, 'exclude', None)

        if fields and fields != ALL_FIELDS and not isinstance(fields, (list, tuple)):
            raise TypeError(
                'The `fields` option must be a list or tuple or "__all__". '
                'Got %s.' % type(fields).__name__
            )

        if exclude and not isinstance(exclude, (list, tuple)):
            raise TypeError(
                'The `exclude` option must be a list or tuple. Got %s.' %
                type(exclude).__name__
            )

        assert not (fields and exclude), (
            "Cannot set both 'fields' and 'exclude' options on "
            "serializer {serializer_class}.".format(
                serializer_class=self.__class__.__name__
            )
        )

        if fields is None and exclude is None:
            warnings.warn(
                "Creating a ModelSerializer without either the 'fields' "
                "attribute or the 'exclude' attribute is deprecated "
                "since 3.3.0. Add an explicit fields = '__all__' to the "
                "{serializer_class} serializer.".format(
                    serializer_class=self.__class__.__name__
                ),
                DeprecationWarning
            )

        if fields == ALL_FIELDS:
            fields = None

        if fields is not None:
            # Ensure that all declared fields have also been included in the
            # `Meta.fields` option.

            # Do not require any fields that are declared a parent class,
            # in order to allow serializer subclasses to only include
            # a subset of fields.
            required_field_names = set(declared_fields)
            for cls in self.__class__.__bases__:
                required_field_names -= set(getattr(cls, '_declared_fields', []))

            for field_name in required_field_names:
                assert field_name in fields, (
                    "The field '{field_name}' was declared on serializer "
                    "{serializer_class}, but has not been included in the "
                    "'fields' option.".format(
                        field_name=field_name,
                        serializer_class=self.__class__.__name__
                    )
                )
            return fields

        # Use the default set of field names if `Meta.fields` is not specified.
        fields = self.get_default_field_names(declared_fields, info)

        if exclude is not None:
            # If `Meta.exclude` is included, then remove those fields.
            for field_name in exclude:
                assert field_name in fields, (
                    "The field '{field_name}' was included on serializer "
                    "{serializer_class} in the 'exclude' option, but does "
                    "not match any model field.".format(
                        field_name=field_name,
                        serializer_class=self.__class__.__name__
                    )
                )
                fields.remove(field_name)

        return fields


    def get_fields(self):
        """
        Return the dict of field names -> field instances that should be
        used for `self.fields` when instantiating the serializer.
        """
        if self.url_field_name is None:
            self.url_field_name = api_settings.URL_FIELD_NAME

        assert hasattr(self, 'Meta'), (
            'Class {serializer_class} missing "Meta" attribute'.format(
                serializer_class=self.__class__.__name__
            )
        )
        assert hasattr(self.Meta, 'model'), (
            'Class {serializer_class} missing "Meta.model" attribute'.format(
                serializer_class=self.__class__.__name__
            )
        )
        if model_meta.is_abstract_model(self.Meta.model):
            raise ValueError(
                'Cannot use ModelSerializer with Abstract Models.'
            )

        declared_fields = copy.deepcopy(self._declared_fields)
        model = getattr(self.Meta, 'model')
        depth = getattr(self.Meta, 'depth', 0)

        if depth is not None:
            assert depth >= 0, "'depth' may not be negative."
            assert depth <= 10, "'depth' may not be greater than 10."

        # Retrieve metadata about fields & relationships on the model class.
        info = model_meta.get_field_info(model)
        field_names = self.get_field_names(declared_fields, info)

        # Determine any extra field arguments and hidden fields that
        # should be included
        extra_kwargs = self.get_extra_kwargs()
        extra_kwargs, hidden_fields = self.get_uniqueness_extra_kwargs(
            field_names, declared_fields, extra_kwargs
        )

        # Determine the fields that should be included on the serializer.
        fields = OrderedDict()

        for field_name in field_names:
            # If the field is explicitly declared on the class then use that.
            if field_name in declared_fields:
                fields[field_name] = declared_fields[field_name]
                continue

            # Determine the serializer field class and keyword arguments.
            field_class, field_kwargs = self.build_field(
                field_name, info, model, depth
            )

            # Include any kwargs defined in `Meta.extra_kwargs`
            extra_field_kwargs = extra_kwargs.get(field_name, {})
            field_kwargs = self.include_extra_kwargs(
                field_kwargs, extra_field_kwargs
            )

            # Create the serializer field.
            fields[field_name] = field_class(**field_kwargs)

        # Add in any hidden fields.
        fields.update(hidden_fields)

        return fields

    # def get_is_new(self, obj):
    #     request = self.context.get('request', None)
    #     if request is not None:
    #         # TODO: Resuelve el problema de cuando se accede a un trhread el usuario que envio el mensaje no debe saber
    #         # si el mensaje es nuevo, osea, el mensaje esta leido o no leido. En el caso que el sender entra al detalle
    #         # del mensaje el atributo is_new es False
    #         if request.user == obj.sender:
    #             return False
    #         else:
    #             return obj.is_new