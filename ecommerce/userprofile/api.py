from rest_framework import viewsets

from .models import Address, User

from .serializers import AddressSerializer, UserSerializer
from rest_framework import filters



class AddressViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing Address
    """
    queryset = Address.objects.all()
    serializer_class = AddressSerializer
    #filter_backends = (filters.DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_fields = ('user',)
    search_fields = ('user__username', 'first_name', 'last_name')
    ordering_fields = '__all__'


class UserViewtSet(viewsets.ModelViewSet):
    """"
    A simple ViewSet for viewwing Users
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    search_fields = ('username', 'first_name', 'last_name')
