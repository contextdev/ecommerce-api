from babeldjango.templatetags.babel import currencyfmt

from rest_framework import serializers

from .models import ShippingMethod, ShippingMethodCountry


class ShippingMethodSerializer(serializers.ModelSerializer):


    class Meta:
        model = ShippingMethod
        fields = '__all__'


class ShippingMethodCountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = ShippingMethodCountry
        fields = '__all__'