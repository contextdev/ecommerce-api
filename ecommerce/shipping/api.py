from rest_framework import viewsets

from .models import ShippingMethod, ShippingMethodCountry

from .serializers import ShippingMethodSerializer, ShippingMethodCountrySerializer


class ShippingMethodViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing ShippingMethods
    """
    queryset = ShippingMethod.objects.all()
    serializer_class = ShippingMethodSerializer

class ShippingMethodCountryViewtSet(viewsets.ModelViewSet):
    """"
    A simple ViewSet for viewwing ShippingMethodCountrys
    """
    queryset = ShippingMethodCountry.objects.all()
    serializer_class = ShippingMethodCountrySerializer
