from rest_framework import viewsets

from .models import Sale, Voucher

from .serializers import SaleSerializer, VoucherSerializer


class SaleViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing Sales
    """
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer

class VoucherViewtSet(viewsets.ModelViewSet):
    """"
    A simple ViewSet for viewwing Vouchers
    """
    queryset = Voucher.objects.all()
    serializer_class = VoucherSerializer
